<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Cache\Cache;
use GuzzleHttp\Client;

class FeedsController extends AppController
{
    /**
     * Generates an RSS feed containing tags for a given docker repo.
     *
     * @param string $owner repo owner
     * @param string $repo name of repo
     * @return void
     */
    public function tags($owner, $repo)
    {
        if ($owner == '_') {
            $owner = 'library';
        }

        $url = "https://hub.docker.com/r/{$owner}/{$repo}/tags/";
        $data = Cache::read($url);

        if (!$data) {
            $client = new Client();
            $result = $client->request('GET', $url);
            if ($result->getStatusCode() != 200) {
                throw new \Exception("Could not fetch feed.");
            }
            $data = strval($result->getBody());
            Cache::write($url, $data);
        }

        $regex = '/window.ReduxApp = ({.*});/s';
        if (!preg_match($regex, $data, $matches)) {
            throw new \Exception("Could not parse feed.");
        }
        $data = json_decode($matches[1], true);
        $this->set(compact('data', 'owner', 'repo'));
    }
}
