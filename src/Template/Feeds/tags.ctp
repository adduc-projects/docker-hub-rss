<?php

use Suin\RSSWriter\Channel;
use Suin\RSSWriter\Feed;
use Suin\RSSWriter\Item;

$tags = $data['tags'][$owner][$repo]['tags'];

usort($tags, function($a, $b) {
    if ($a['last_updated'] == $b['last_updated']) {
        return strcmp($b['name'], $a['name']);
    }
    
    return strtotime($b['last_updated']) - strtotime($a['last_updated']);
});

$feed = new Feed();

$channel = new Channel();
$channel
    ->title("Tags from {$owner}/{$repo} - Docker Hub Repo")
    ->description($data['repos']['description'])
    ->url("https://hub.docker.com/r/{$owner}/{$repo}/tags/")
    ->lastBuildDate(time())
    ->ttl(3600)
    ->appendTo($feed);
    
foreach ($tags as $tag) {
    
    $description
        = "Image: {$owner}/{$repo}:{$tag['name']}<br>"
        . "Size: " . number_format($tag['full_size'] / 1000 / 1000, 2) . " MB<br>";
    
    $item = new Item();
    $item
        ->title($tag['name'])
        ->contentEncoded($description)
        ->url("https://hub.docker.com/r/{$owner}/{$repo}/tags/")
        ->pubDate(strtotime($tag['last_updated']))
        ->guid($tag['last_updated'] . '-' . $tag['name'], false)
        ->appendTo($channel);
}

$this->autoLayout = false;
$this->response->body($feed->render());
$this->response->type('xml');
echo $this->response;
