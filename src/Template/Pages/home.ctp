<!DOCTYPE html>
<html>
<head>
    <title>Docker Hub RSS</title>
</head>
<body>
    <h1>Docker Hub RSS</h1>

    <h2>Examples:</h2>

    <?php

    $urls = [
        [
            'controller' => 'feeds',
            'action' => 'tags',
            'gitlab',
            'gitlab-ce'
        ],
        [
            'controller' => 'feeds',
            'action' => 'tags',
            'library',
            'debian'
        ]
    ];
    ?>

    <ul>
        <?php foreach ($urls as $url) { ?>
        <li>
            <a href="<?=$this->Url->build($url, true)?>">
                <?=$this->Url->build($url, true)?>
            </a>
        </li>
        <?php } ?>
    </ul>
</body>
</html>
